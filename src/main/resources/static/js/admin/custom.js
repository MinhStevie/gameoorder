var editor;

$(document).ready(function() {
 $('#dataTables').DataTable({
	 // columnDefs: [ {
		//  className: 'select-checkbox',
		//  targets:   0
	 // } ],
	 // select: {
		//  style:    'os',
		//  selector: 'td:first-child'
	 // },
	 // order: [[ 1, 'asc' ]],
	 dom : 'Bfrtip',
 buttons : [ 'excel','pdf' ]

 });

	
	  // Sidebar links
	  $('.sidebar .sidebar-menu li a').on('click', function () {
	    const $this = $(this);

	    if ($this.parent().hasClass('open')) {
	      $this
	        .parent()
	        .children('.dropdown-menu')
	        .slideUp(200, () => {
	          $this.parent().removeClass('open');
	        });
	    } else {
	      $this
	        .parent()
	        .parent()
	        .children('li.open')
	        .children('.dropdown-menu')
	        .slideUp(200);

	      $this
	        .parent()
	        .parent()
	        .children('li.open')
	        .children('a')
	        .removeClass('open');

	      $this
	        .parent()
	        .parent()
	        .children('li.open')
	        .removeClass('open');

	      $this
	        .parent()
	        .children('.dropdown-menu')
	        .slideDown(200, () => {
	          $this.parent().addClass('open');
	        });
	    }
	  });

	  // Sidebar Activity Class
	  const sidebarLinks = $('.sidebar').find('.sidebar-link');

	  sidebarLinks
	    .each((index, el) => {
	      $(el).removeClass('active');
	    })
	    .filter(function () {
	      const href = $(this).attr('href');
	      const pattern = href[0] === '/' ? href.substr(1) : href;
	      return pattern === (window.location.pathname).substr(1);
	    })
	    .addClass('active');

	  // ٍSidebar Toggle
	  $('.sidebar-toggle').on('click', e => {
	    $('.app').toggleClass('is-collapsed');
	    e.preventDefault();
	  });

	  /**
		 * Wait untill sidebar fully toggled (animated in/out) then trigger
		 * window resize event in order to recalculate masonry layout widths and
		 * gutters.
		 */
	  window.addEventListener('load', () => {
	    if ($('.masonry').length > 0) {
	      new Masonry('.masonry', {
	        itemSelector: '.masonry-item',
	        columnWidth: '.masonry-sizer',
	        percentPosition: true,
	      });
	    }
	  });
	  
	  $('.search-toggle').on('click', e => {
		    $('.search-box, .search-input').toggleClass('active');
		    $('.search-input input').focus();
		    e.preventDefault();
		  });
		  
});