package com.authentic.gameo.controller;

import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Inventory;
import com.authentic.gameo.entity.UploadForm;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.service.CategoryService;
import com.authentic.gameo.service.InventoryService;
import com.authentic.gameo.service.ProductService;
import com.authentic.gameo.utils.Excel;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class InventoryImportController {

    @Autowired private InventoryService inventoryService;
    @Autowired private ProductService productService;
    @Autowired private CategoryService categoryService;
    @Autowired private BrandService brandService;

    private static Logger logger = Logger.getLogger(InventoryImportController.class.getName());
//    // GET: Hiển thị trang form upload
//    @RequestMapping(value = "/importcategory", method = RequestMethod.GET)
//    public String importcategoryHandler(Model model) {
//
//        model.addAttribute("uploadForm", new UploadForm());
//
//        return "importcategory";
//    }

    // POST: Sử lý Upload
    @RequestMapping(value = "/importinventory", method = RequestMethod.POST)
    public String importcategoryHandlerPOST(HttpServletRequest request, //
                                           Model model, //
                                           @ModelAttribute("uploadForm") UploadForm uploadForm
    ,RedirectAttributes redirectAttributes) {

        return this.doUpload(request, model, uploadForm,redirectAttributes);

    }

    private String doUpload(HttpServletRequest request, Model model, //
                            UploadForm uploadForm,RedirectAttributes redirectAttributes) {

        // Thư mục gốc upload file.
//        String uploadRootPath = request.getServletContext().getRealPath("upload");
//        String uploadRootPath = "/Users/sato/intellijProjects/SpringBootFileUpload/src/main/resources/static";
//        String uploadRootPath = "C:\\Users\\minh_hd\\IdeaProjects\\gameoorder-master\\upload" ;
        String uploadRootPath ="upload";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
        LocalDateTime currentDateTime = LocalDateTime.now();
//        currentDateTime.format(formatter)
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }

        MultipartFile[] fileDatas = uploadForm.getFileDatas();
        //
        List<File> uploadedFiles = new ArrayList<File>();
        List<String> failedFiles = new ArrayList<String>();
        String transactions = "";

        for (MultipartFile fileData : fileDatas) {

            // Tên file gốc tại Client.
            String name = fileData.getOriginalFilename();

            if (name != null && name.length() > 0) {
                try {
                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

                    if (serverFile.exists()) {

                        serverFile = new File(serverFile.getAbsolutePath().replace(".","-" + currentDateTime.format(formatter) + "."));
                    }

                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(fileData.getBytes());
                    stream.close();
                    //

                    if (serverFile.getAbsolutePath().contains(".xls") || serverFile.getAbsolutePath().contains(".xlsx")) {
                        uploadedFiles.add(serverFile);
                    } else {
                        failedFiles.add(name);
                        transactions = "file " + name + " không đúng định dạng excel";
                    }

                } catch (Exception e) {
                    logger.info("Error Write file: " + name + " with error " + e.toString());
                    failedFiles.add(name);
                }
            }
        }


        if (failedFiles == null) {
            FileInputStream fileInputStream = null;
//        HSSFWorkbook workbook = null;
            XSSFWorkbook workbook = null;
            List<Inventory> inventoryList = new ArrayList<>();
            List<String> errors = new ArrayList<>();


            for (int i = 0; i < uploadedFiles.size(); i++) {
                transactions += "File " + uploadedFiles.get(i);
                try {
                    fileInputStream = new FileInputStream(uploadedFiles.get(0));
                    workbook = new XSSFWorkbook(fileInputStream);
                    XSSFSheet sheet = workbook.getSheetAt(0);

                    Iterator<Row> rowIterator = sheet.iterator();
                    // By pass header
                    rowIterator.next();

                    while (rowIterator.hasNext()) {
                        Inventory inventory = new Inventory();
                        Row row = rowIterator.next();

                        String setBrand = String.valueOf(Excel.getCellValue(row.getCell(0)));
                        String setCategory = String.valueOf(Excel.getCellValue(row.getCell(1)));
                        String setProduct = String.valueOf(Excel.getCellValue(row.getCell(2)));
                        String setAvailablePrice = String.valueOf(Excel.getCellValue(row.getCell(3)));
                        String setImportPrice = String.valueOf(Excel.getCellValue(row.getCell(4)));
                        String setSalePrice = String.valueOf(Excel.getCellValue(row.getCell(5)));

                        inventory.setBrand(brandService.getOne(Integer.valueOf(setBrand)));
                        inventory.setProduct(productService.getOne(Integer.valueOf(setProduct)));
                        inventory.setCategory(categoryService.getOne(Integer.valueOf(setCategory)));
                        inventory.setInsertDate(LocalDateTime.now());
                        inventory.setAvailablePrice(Integer.valueOf(setAvailablePrice));
                        inventory.setImportPrice(Integer.valueOf(setImportPrice));
                        inventory.setSalePrice(Integer.valueOf(setSalePrice));

                        inventory.setOrderCode(String.valueOf(Excel.getCellValue(row.getCell(6))));

                        inventoryList.add(inventory);

                    }

                    List<Inventory> listSuccess = inventoryService.saveAll(inventoryList);
                    transactions += " success " + listSuccess.size();

                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info(" lỗi " + e.toString());
                }
            }
        }
        redirectAttributes.addFlashAttribute("uploadedFiles",uploadedFiles);
        redirectAttributes.addFlashAttribute("failedFiles",failedFiles);
        redirectAttributes.addFlashAttribute("transactions",transactions);

        return "redirect:/categorylist";
//        return "category/categorylist";
    }


}
