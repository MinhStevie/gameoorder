package com.authentic.gameo.controller;

import com.authentic.gameo.entity.Brand;
import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Product;
import com.authentic.gameo.entity.UploadForm;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.service.CategoryService;
import com.authentic.gameo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class ProductController {
    @Autowired private ProductService productService;
    @Autowired private CategoryService categoryService;
    @Autowired private BrandService brandService;

    @RequestMapping("/productlist")
    public String index(Model model) {
        List<Product> products = productService.getAllProduct();

        model.addAttribute("products", products);
        model.addAttribute("uploadForm", new UploadForm());

        return "pages/productList";
    }

    @RequestMapping(value = "/addproduct")
    public String addProduct(Model model) {
        model.addAttribute("product", new Product());

        List<Brand> brands = brandService.getAllBrand();
        model.addAttribute("brands", brands);

        List<Category> categorys = categoryService.getAllCategory();
        model.addAttribute("categorys", categorys);

        return "pages/productAdd";
    }

    @RequestMapping(value = "/editproduct", method = RequestMethod.GET)
    public String editProduct(@RequestParam("id") Integer productId, Model model) {
        Optional<Product> productEdit = productService.findProductById(productId);
        productEdit.ifPresent(product -> model.addAttribute("product", product));

        List<Brand> brands = brandService.getAllBrand();
        model.addAttribute("brands", brands);

        List<Category> categorys = categoryService.getAllCategory();
        model.addAttribute("categorys", categorys);

        return "pages/productEdit";
    }

    @RequestMapping(value = "/saveproduct", method = RequestMethod.POST)
    public String save(@RequestParam("selectedbrand") String selectedbrand,
                       @RequestParam("selectedcategory") String selectedcategory,
                       Product product, RedirectAttributes redirectAttributes) {

        List<String> transactions = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        if (product.getProductId() == null && productService.existsProductByProductName(product.getProductName())) {
            errors.add(product.getProductName() + " đã tồn tại");
        } else {
            product.setBrand(brandService.getOne(Integer.valueOf(selectedbrand)));
            product.setCategory(categoryService.getOne(Integer.valueOf(selectedcategory)));
            productService.saveProduct(product);
            transactions.add(" Thêm mới thành công sản phẩm " + product.getProductName());
        }
        redirectAttributes.addFlashAttribute("transactions",transactions);
        redirectAttributes.addFlashAttribute("errors",errors);

        return "redirect:/productlist";
    }

    @RequestMapping(value = "/deleteproduct", method = RequestMethod.GET)
    public String prepareDelete(@RequestParam("id") Integer productId, Model model) {

        model.addAttribute("deleteId", productId);

        return "pages/productDelete";

    }

    @RequestMapping(value = "/deleteproduct", method = RequestMethod.POST)
    public String processDelete(@RequestParam("deleteId") Integer productId
            , RedirectAttributes redirectAttributes) {

        productService.deleteProduct(productId);
        redirectAttributes.addFlashAttribute("transactions", "Success delete records " + productId);

        return "redirect:/productlist";
    }
}