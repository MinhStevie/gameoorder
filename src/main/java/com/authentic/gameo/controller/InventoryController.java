package com.authentic.gameo.controller;

import com.authentic.gameo.entity.*;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.service.CategoryService;
import com.authentic.gameo.service.InventoryService;
import com.authentic.gameo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class InventoryController {
    @Autowired private InventoryService inventoryService;
    @Autowired private ProductService productService;
    @Autowired private CategoryService categoryService;
    @Autowired private BrandService brandService;

    @RequestMapping("/inventorylist")
    public String index(Model model) {
        List<Inventory> inventorys = inventoryService.getAllInventory();
        model.addAttribute("inventorys", inventorys);
        model.addAttribute("uploadForm", new UploadForm());

        return "pages/inventoryList";
    }

    @RequestMapping(value = "/addinventory")
    public String addInventory(Model model) {
        model.addAttribute("inventory", new Inventory());
        model.addAttribute("categorys", categoryService.getAllCategory());
        model.addAttribute("products", productService.getAllProduct());
        model.addAttribute("brands", brandService.getAllBrand());

        return "pages/inventoryAdd";
    }

    @RequestMapping(value = "/editinventory", method = RequestMethod.GET)
    public String editInventory(@RequestParam("id") Integer inventoryId, Model model) {
        Optional<Inventory> inventoryEdit = inventoryService.findInventoryById(inventoryId);
        inventoryEdit.ifPresent(inventory -> model.addAttribute("inventory", inventory));

        model.addAttribute("categorys", categoryService.getAllCategory());
        model.addAttribute("products", productService.getAllProduct());
        model.addAttribute("brands", brandService.getAllBrand());

        return "pages/inventoryEdit";
    }

    @RequestMapping(value = "/saveinventory", method = RequestMethod.POST)
    public String save(@RequestParam("selectedbrand") String selectedbrand,
                       @RequestParam("selectedcategory") String selectedcategory,
                       @RequestParam("selectedproduct") String selectedproduct,
                       Inventory inventory) {

        inventory.setBrand(brandService.getOne(Integer.valueOf(selectedbrand)));
        inventory.setCategory(categoryService.getOne(Integer.valueOf(selectedcategory)));
        inventory.setProduct(productService.getOne(Integer.valueOf(selectedproduct)));

        inventoryService.saveInventory(inventory);

        return "redirect:/inventorylist";
    }

    @RequestMapping(value = "/deleteinventory", method = RequestMethod.GET)
    public String deleteInventory(@RequestParam("id") Integer inventoryId, Model model) {
        inventoryService.deleteInventory(inventoryId);
        return "redirect:/inventorylist";
    }
}