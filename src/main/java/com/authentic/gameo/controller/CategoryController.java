package com.authentic.gameo.controller;

import com.authentic.gameo.entity.Brand;
import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.UploadForm;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class CategoryController {
    @Autowired private CategoryService categoryService;
    @Autowired private BrandService brandService;

    @RequestMapping("/categorylist")
    public String index(Model model) {
        List<Category> category = categoryService.getAllCategory();

        model.addAttribute("category", category);

        return "pages/categoryList";
    }

    @RequestMapping(value = "/addcategory")
    public String addCategory(Model model) {

        model.addAttribute("category", new Category());
        List<Brand> brands = brandService.getAllBrand();
        model.addAttribute("brands", brands);

        return "pages/categoryAdd";
    }

    @RequestMapping(value = "/editcategory", method = RequestMethod.GET)
    public String editCategory(@RequestParam("id") Integer categoryId, Model model) {
        Optional<Category> categoryEdit = categoryService.findCategoryById(categoryId);
        categoryEdit.ifPresent(category -> model.addAttribute("category", category));

        List<Brand> brands = brandService.getAllBrand();
        model.addAttribute("brands", brands);

        return "pages/categoryEdit";
    }

    @RequestMapping(value = "/savecategory", method = RequestMethod.POST)
    public String save(@RequestParam("selectedbrand") String selectedbrand,Category category, RedirectAttributes redirectAttributes) {
        List<String> transactions = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        if (categoryService.existsCategoryByCategoryName(category.getCategoryName())) {
            errors.add(category.getCategoryName() + " đã tồn tại");
        } else {
            category.setBrand(brandService.getOne(Integer.valueOf(selectedbrand)));
            categoryService.saveCategory(category);
            transactions.add(" Thêm mới thành công mặt hàng " + category.getCategoryName());
        }
        redirectAttributes.addFlashAttribute("transactions",transactions);
        redirectAttributes.addFlashAttribute("errors",errors);
        return "redirect:/categorylist";
    }

    @RequestMapping(value = "/deletecategory", method = RequestMethod.GET)
    public String prepareDelete(@RequestParam("id") Integer categoryId
            , Model model) {

        model.addAttribute("deleteId", categoryId);

        return "pages/categoryDelete";
    }

    @RequestMapping(value = "/deletecategory", method = RequestMethod.POST)
    public String deleteCategory(@RequestParam("deleteId") Integer categoryId
            , RedirectAttributes redirectAttributes) {
        categoryService.deleteCategory(categoryId);
        redirectAttributes.addFlashAttribute("transactions", "Xóa thành công bản ghi " + categoryId);
        return "redirect:/categorylist";
    }

}