package com.authentic.gameo.controller;

import com.authentic.gameo.entity.Brand;
import com.authentic.gameo.entity.UploadForm;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.utils.EncrytedPasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class BrandController {
    @Autowired private BrandService brandService;

    @RequestMapping("/brandlist")
    public String index(Model model) {
        List<Brand> brands = brandService.getAllBrand();

        model.addAttribute("brands", brands);
        model.addAttribute("uploadForm", new UploadForm());

        return "pages/brandList";
    }

    @RequestMapping(value = "/addbrand")
    public String addBrand(Model model) {
        model.addAttribute("brand", new Brand());
        return "pages/brandAdd";
    }

    @RequestMapping(value = "/editbrand", method = RequestMethod.GET)
    public String editBrand(@RequestParam("id") Integer brandId, Model model) {
        Optional<Brand> brandEdit = brandService.findBrandById(brandId);
        brandEdit.ifPresent(brand -> model.addAttribute("brand", brand));
        return "pages/brandEdit";
    }

    @RequestMapping(value = "/savebrand", method = RequestMethod.POST)
    public String save(Brand brand, RedirectAttributes redirectAttributes) {

        List<String> transactions = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        if (brandService.existsBrandByBrandName(brand.getBrandName())) {
            errors.add(brand.getBrandName() + " đã tồn tại");
        } else {
            brandService.saveBrand(brand);
            transactions.add(" Thêm mới thành công nhãn hàng " + brand.getBrandName());
        }
        redirectAttributes.addFlashAttribute("transactions",transactions);
        redirectAttributes.addFlashAttribute("errors",errors);
        return "redirect:/brandlist";
    }

    @RequestMapping(value = "/deletebrand", method = RequestMethod.GET)
    public String prepareDelete(@RequestParam("id") Integer brandId
            , Model model) {

        model.addAttribute("deleteId", brandId);

        return "pages/brandDelete";
    }

    @RequestMapping(value = "/deletebrand", method = RequestMethod.POST)
    public String deleteBrand(@RequestParam("deleteId") Integer brandId, RedirectAttributes redirectAttributes) {
        brandService.deleteBrand(brandId);
        redirectAttributes.addFlashAttribute("transactions", "Xóa thành công bản ghi " + brandId);
        return "redirect:/brandlist";
    }

}