package com.authentic.gameo.controller;

import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Product;
import com.authentic.gameo.entity.UploadForm;
import com.authentic.gameo.service.BrandService;
import com.authentic.gameo.service.CategoryService;
import com.authentic.gameo.service.ProductService;
import com.authentic.gameo.utils.Excel;
import com.authentic.gameo.utils.WebUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class ProductImportController {

    @Autowired private ProductService productService;
    @Autowired private CategoryService categoryService;
    @Autowired private BrandService brandService;

    private static Logger logger = Logger.getLogger(ProductImportController.class.getName());
//    // GET: Hiển thị trang form upload
    @RequestMapping(value = "/importproduct", method = RequestMethod.GET)
    public String importproductHandler(Model model) {

        return "pages/productImport";
    }

    // POST: Sử lý Upload
    @RequestMapping(value = "/importproduct", method = RequestMethod.POST)
    public String importproductHandlerPOST(HttpServletRequest request, //
                                           Model model, //
                                           @RequestParam("MultipartFile") MultipartFile[] fileName
    ,RedirectAttributes redirectAttributes) {

        return this.doUpload(request, model, fileName,redirectAttributes);

    }

    private String doUpload(HttpServletRequest request, Model model, //
                            MultipartFile[] fileDatas,RedirectAttributes redirectAttributes) {

        // Thư mục gốc upload file.
//        String uploadRootPath = request.getServletContext().getRealPath("upload");


        //
        List<File> uploadedFiles = new ArrayList<>();
        List<String> failedFiles = new ArrayList<>();
        List<String> transactions = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        for (MultipartFile fileData : fileDatas) {

            File serverFile = WebUtils.uploadFile(fileData) ;

            if (serverFile == null) {
                failedFiles.add(fileData.getOriginalFilename());
            } else if (serverFile.getAbsolutePath().contains(".xls") || serverFile.getAbsolutePath().contains(".xlsx")) {
                uploadedFiles.add(serverFile);
            } else {
                failedFiles.add(fileData.getOriginalFilename());
                errors.add("file " + fileData.getOriginalFilename() + " không đúng định dạng excel") ;
            }
        }
        transactions.add("upload hoành thành. Thành công :" + uploadedFiles.size() + ". Lỗi :" + failedFiles.size() + ".");

        if (uploadedFiles.isEmpty() == false) {
            FileInputStream fileInputStream = null;
//        HSSFWorkbook workbook = null;
            XSSFWorkbook workbook = null;
            List<Product> productList = new ArrayList<>();
            List<String> existsRecords = new ArrayList<>();


            for (int i = 0; i < uploadedFiles.size(); i++) {

                try {
                    fileInputStream = new FileInputStream(uploadedFiles.get(0));
                    workbook = new XSSFWorkbook(fileInputStream);
                    XSSFSheet sheet = workbook.getSheetAt(0);

                    Iterator<Row> rowIterator = sheet.iterator();
                    // By pass header
                    rowIterator.next();

                    while (rowIterator.hasNext()) {
                        Product product = new Product();
                        Row row = rowIterator.next();

                        String setProductName = String.valueOf(Excel.getCellValue(row.getCell(0)));
                        String setBrand = String.valueOf(Excel.getCellValue(row.getCell(1)));
                        String setCategory = String.valueOf(Excel.getCellValue(row.getCell(2)));
                        String setProductPrice = String.valueOf(Excel.getCellValue(row.getCell(3)));

                        if (productService.existsProductByProductName(setProductName)) {

                            existsRecords.add(setProductName);

                        } else {

                            product.setProductName(setProductName);
                            product.setBrand(brandService.getOne(Integer.valueOf(setBrand)));
                            product.setCategory(categoryService.getOne(Integer.valueOf(setCategory)));
                            product.setInsertDate(LocalDateTime.now());
                            product.setProductPrice(Integer.valueOf(setProductPrice));

                            if (product.getProductName().contains("java.lang.Object@")) {
                                errors.add(product.getProductName() + " không đúng định dạng tại " + row.getRowNum());
                                break;
                            }

                            productList.add(product);
                        }

                    }

                    List<Product> listSuccess = productService.saveAll(productList);
                    transactions.add("Thêm mới " + listSuccess.size() + " mặt hàng.");
                    if (existsRecords.size() > 0) {
                        errors.add("Có " + existsRecords.size() + " mặt hàng đã tồn tại :" + existsRecords);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    logger.info(" lỗi " + e.toString());
                }
            }
        }

        redirectAttributes.addFlashAttribute("transactions",transactions);
        redirectAttributes.addFlashAttribute("errors",errors);

        return "redirect:/productlist";
//        return "product/productlist";
    }


}
