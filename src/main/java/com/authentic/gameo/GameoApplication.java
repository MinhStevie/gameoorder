package com.authentic.gameo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GameoApplication.class, args);
    }

}
