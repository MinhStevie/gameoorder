package com.authentic.gameo.utils;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

import java.util.logging.Logger;

public class Excel {
    private static Logger logger = Logger.getLogger(Excel.class.getName());

    public static Object getCellValue(Cell cell) {
        Object obj = new Object();
        try {

            if (cell.getCellType() == CellType.STRING) {

                obj = cell.getStringCellValue();

            } else if (cell.getCellType() == CellType.BOOLEAN) {

                obj = cell.getBooleanCellValue();

            } else if (cell.getCellType() == CellType.NUMERIC) {

                obj = Math.round(cell.getNumericCellValue());

                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    obj = cell.getDateCellValue();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString());
        }
        return obj;
    }

}
