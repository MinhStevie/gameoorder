package com.authentic.gameo.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EncrytedPasswordUtils {

    // Encryte Password with BCryptPasswordEncoder
    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }

    public static void main(String[] args) {
        String password = "123";
        String encrytedPassword = encrytePassword(password);

        System.out.println("Encryted Password: " + encrytedPassword);

        // Get current date time
        LocalDateTime currentDateTime = LocalDateTime.now();

// Custom format
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");

// Format LocalDateTime
        String formattedDateTime = currentDateTime.format(formatter);

//Verify
        System.out.println("Formatted LocalDateTime : " + formattedDateTime);

//Output:

//Formatted LocalDateTime : 2018-07-14T17:45:55.9483536
    }

}