package com.authentic.gameo.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.logging.Logger;

import com.authentic.gameo.controller.CategoryImportController;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.multipart.MultipartFile;

public class WebUtils {
    private static Logger logger = Logger.getLogger(WebUtils.class.getName());

    public static File uploadFile(MultipartFile fileData) {

        File serverFile = null ;

        String uploadRootPath ="upload";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
        LocalDateTime currentDateTime = LocalDateTime.now();
//        currentDateTime.format(formatter)
        File uploadRootDir = new File(uploadRootPath);
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }

        // Tên file gốc tại Client.
        String name = fileData.getOriginalFilename();

        if (name != null && name.length() > 0) {
            try {
                // Tạo file tại Server.
                serverFile = new File(uploadRootDir.getAbsolutePath() + File.separator + name);

                if (serverFile.exists()) {

                    serverFile = new File(serverFile.getAbsolutePath().replace(".","-" + currentDateTime.format(formatter) + "."));
                }

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                stream.write(fileData.getBytes());
                stream.close();
                //

            } catch (Exception e) {
                logger.info("Error Write file: " + name + " with error " + e.toString());
            }
        }

        return serverFile;
    }

    public static String toString(User user) {
        StringBuilder sb = new StringBuilder();

        sb.append("UserName:").append(user.getUsername());

        Collection<GrantedAuthority> authorities = user.getAuthorities();
        if (authorities != null && !authorities.isEmpty()) {
            sb.append(" (");
            boolean first = true;
            for (GrantedAuthority a : authorities) {
                if (first) {
                    sb.append(a.getAuthority());
                    first = false;
                } else {
                    sb.append(", ").append(a.getAuthority());
                }
            }
            sb.append(")");
        }
        return sb.toString();
    }

}