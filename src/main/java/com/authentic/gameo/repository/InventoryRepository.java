package com.authentic.gameo.repository;

import com.authentic.gameo.entity.AppUser;
import com.authentic.gameo.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Integer> {}
