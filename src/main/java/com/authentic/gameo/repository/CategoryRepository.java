package com.authentic.gameo.repository;

import com.authentic.gameo.entity.Category;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>, CrudRepository<Category, Integer> {

    Category findCategoryByCategoryName(String categoryname) ;

    boolean existsCategoryByCategoryName(String categoryname);
}
