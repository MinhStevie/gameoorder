package com.authentic.gameo.repository;

import com.authentic.gameo.entity.AppUser;
import com.authentic.gameo.entity.Brand;
import com.authentic.gameo.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer>,CrudRepository<Brand, Integer> {
    boolean existsBrandByBrandName(String brandname);
}
