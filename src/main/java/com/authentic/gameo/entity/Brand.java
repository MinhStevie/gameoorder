package com.authentic.gameo.entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


@Entity
@Table(name = "Brand")
public class Brand {

    @Id
    @GeneratedValue
    @Column(name = "brandid", length = 11, nullable = false)
    private Integer brandId;

    @Column(name = "brandname", length = 50, nullable = false,unique=true)
    private String brandName;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "insertdate", columnDefinition = "DATE")
    private LocalDateTime insertDate;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "modifieddate", columnDefinition = "DATE")
    private LocalDateTime modifiedDate;

    @Column(name = "brandimage", length = 100)
    private String brandImage;

    @Column(name = "branddesc", length = 100)
    private String brandDesc;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public LocalDateTime getInsertDate() { return insertDate; }

    public void setInsertDate(LocalDateTime insertDate) {
        if (insertDate == null) {
            insertDate = LocalDateTime.now();
        }

        this.insertDate = insertDate ;
    }

    public LocalDateTime getModifiedDate() { return modifiedDate ; }

    public void setModifiedDate(LocalDateTime modifiedDate) { this.modifiedDate = LocalDateTime.now(); }

    public String getBrandImage() {
        return brandImage;
    }

    public void setBrandImage(String brandImage) {
        this.brandImage = brandImage;
    }


    public String getBrandDesc() {
        return brandDesc;
    }

    public void setBrandDesc(String brandDesc) {
        this.brandDesc = brandDesc;
    }
}
