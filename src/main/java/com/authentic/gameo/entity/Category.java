package com.authentic.gameo.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.TemporalType.DATE;

@Entity
@Table(name = "Category")
public class Category {

    @Id
    @GeneratedValue
    @Column(name = "categoryid", length = 11, nullable = false)
    private Integer categoryId;

    @Column(name = "categoryname", length = 50, nullable = false,unique=true)
    private String categoryName;

    @Column(name = "categoryprice", length = 11, nullable = false)
    private Integer categoryPrice;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brandid")
    private Brand brand;

    @Column(name = "categoryimage", length = 100)
    private String categoryImage;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "insertdate", columnDefinition = "DATE")
    private LocalDateTime insertDate;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "modifieddate", columnDefinition = "DATE")
    private LocalDateTime modifiedDate;

    @Column(name = "categorydesc", length = 100)
    private String categoryDesc;

    public Integer getCategoryPrice() {
        return categoryPrice;
    }

    public void setCategoryPrice(Integer categoryPrice) {
        this.categoryPrice = categoryPrice;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public LocalDateTime getInsertDate() { return insertDate; }

    public void setInsertDate(LocalDateTime insertDate) {
        if (insertDate == null) {
            insertDate = LocalDateTime.now();
        }

        this.insertDate = insertDate ;
    }

    public LocalDateTime getModifiedDate() { return modifiedDate ; }

    public void setModifiedDate(LocalDateTime modifiedDate) { this.modifiedDate = LocalDateTime.now(); }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }
}
