package com.authentic.gameo.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static javax.persistence.TemporalType.DATE;

@Entity
@Table(name = "Inventory")
public class Inventory {
    @Id
    @GeneratedValue
    @Column(name = "inventoryid", nullable = false)
    private Integer inventoryId;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "categoryid")
    private Category category;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brandid")
    private Brand brand;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "productid")
    private Product product;

    @Column(name = "ordercode", length = 11, nullable = false)
    private String orderCode;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "insertdate", columnDefinition = "DATE")
    private LocalDateTime insertDate;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "modifieddate", columnDefinition = "DATE")
    private LocalDateTime modifiedDate;

    @Column(name = "orderdate", columnDefinition = "DATE")
    private LocalDateTime orderDate;

    @Column(name = "importprice", length = 11, nullable = false)
    private Integer importPrice;

    @Column(name = "availableprice", length = 11, nullable = false)
    private Integer availablePrice;

    @Column(name = "saleprice", length = 11, nullable = false)
    private Integer salePrice;

    @Column(name = "status", length = 1, nullable = false)
    private boolean status;

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getOrderCode() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime currentDateTime = LocalDateTime.now();

        return orderCode == null ? "GS-" +currentDateTime.format(formatter) : orderCode;
    }

    public void setOrderCode(String orderCode) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime currentDateTime = LocalDateTime.now();
        this.orderCode = orderCode == null ? "GS-" +currentDateTime.format(formatter) : orderCode;
    }

    public Integer getImportPrice() {
        return importPrice;
    }

    public void setImportPrice(Integer importPrice) {
        this.importPrice = importPrice;
    }

    public Integer getAvailablePrice() {
        return availablePrice;
    }

    public void setAvailablePrice(Integer availablePrice) {
        this.availablePrice = availablePrice;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public LocalDateTime getInsertDate() { return insertDate; }

    public void setInsertDate(LocalDateTime insertDate) {
        if (insertDate == null) {
            insertDate = LocalDateTime.now();
        }

        this.insertDate = insertDate ;
    }

    public LocalDateTime getModifiedDate() { return modifiedDate ; }

    public void setModifiedDate(LocalDateTime modifiedDate) { this.modifiedDate = LocalDateTime.now(); }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
