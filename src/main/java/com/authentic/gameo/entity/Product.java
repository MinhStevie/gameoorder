package com.authentic.gameo.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.TemporalType.DATE;

@Entity
@Table(name = "Product")
public class Product {

    @Id
    @GeneratedValue
    @Column(name = "productid", nullable = false)
    private Integer productId;

    @Column(name = "productprice")
    private Integer productPrice;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "categoryid")
    private Category category;

    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "brandid")
    private Brand brand;

    @Column(name = "productName",unique=true)
    private String productName;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "insertdate", columnDefinition = "DATE")
    private LocalDateTime insertDate;

    @DateTimeFormat(pattern="dd-MM-yyyy hh:mm:ss a.")
    @Column(name = "modifieddate", columnDefinition = "DATE")
    private LocalDateTime modifiedDate;

    public Integer getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Integer productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public LocalDateTime getInsertDate() { return insertDate; }

    public void setInsertDate(LocalDateTime insertDate) {
        if (insertDate == null) {
            insertDate = LocalDateTime.now();
        }

        this.insertDate = insertDate ;
    }

    public LocalDateTime getModifiedDate() { return modifiedDate ; }

    public void setModifiedDate(LocalDateTime modifiedDate) { this.modifiedDate = LocalDateTime.now(); }
}
