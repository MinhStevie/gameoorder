package com.authentic.gameo.service;

import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Inventory;

import java.util.List;
import java.util.Optional;

public interface InventoryService {
    List<Inventory> getAllInventory();

    void saveInventory(Inventory inventory);

    void deleteInventory(Integer id);

    Optional<Inventory> findInventoryById(Integer id);

    List<Inventory> saveAll(List<Inventory> inventory);
}