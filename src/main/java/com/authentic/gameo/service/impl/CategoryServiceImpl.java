package com.authentic.gameo.service.impl;

import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Category;
import com.authentic.gameo.repository.CategoryRepository;
import com.authentic.gameo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategory() {
        return (List<Category>) categoryRepository.findAll();
    }

    @Override
    public void saveCategory(Category user) {

        categoryRepository.save(user);
    }

    @Override
    public void deleteCategory(Integer id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public Optional<Category> findCategoryById(Integer id) {
        return categoryRepository.findById(id);
    }

    @Override
    public Category getOne(Integer id) {
        return categoryRepository.getOne(id);
    }

    @Override
    public boolean existsCategoryByCategoryName(String categoryname) {
        return categoryRepository.existsCategoryByCategoryName(categoryname);
    }

    @Override
    public List<Category> saveAll(List<Category> cateogry) {
        return categoryRepository.saveAll(cateogry);
    }
}