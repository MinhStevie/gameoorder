package com.authentic.gameo.service.impl;

import com.authentic.gameo.entity.Inventory;
import com.authentic.gameo.entity.Inventory;
import com.authentic.gameo.repository.InventoryRepository;
import com.authentic.gameo.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryServiceImpl implements InventoryService {
    
    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public List<Inventory> getAllInventory() {
        return (List<Inventory>) inventoryRepository.findAll();
    }

    @Override
    public void saveInventory(Inventory inventory) {
        inventoryRepository.save(inventory);
    }

    @Override
    public void deleteInventory(Integer id) {
        inventoryRepository.deleteById(id);
    }

    @Override
    public Optional<Inventory> findInventoryById(Integer id) {
        return inventoryRepository.findById(id);
    }

    @Override
    public List<Inventory> saveAll(List<Inventory> inventory) {
        return inventoryRepository.saveAll(inventory);
    }
}