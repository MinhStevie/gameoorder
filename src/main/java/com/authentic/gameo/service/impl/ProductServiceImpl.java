package com.authentic.gameo.service.impl;

import com.authentic.gameo.entity.Product;
import com.authentic.gameo.entity.Product;
import com.authentic.gameo.repository.ProductRepository;
import com.authentic.gameo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAllProduct() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public void saveProduct(Product user) {
        productRepository.save(user);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public Optional<Product> findProductById(Integer id) {
        return productRepository.findById(id);
    }

    @Override
    public Product getOne(Integer id) {
        return productRepository.getOne(id);
    }

    @Override
    public List<Product> saveAll(List<Product> product) {
        return productRepository.saveAll(product);
    }

    @Override
    public boolean existsProductByProductName(String product) {
        return productRepository.existsProductByProductName(product);
    }
}