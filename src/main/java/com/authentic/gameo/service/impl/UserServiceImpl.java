package com.authentic.gameo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import com.authentic.gameo.entity.AppUser;
import com.authentic.gameo.repository.UserRepository;
import com.authentic.gameo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<AppUser> getAllUser() {
        return (List<AppUser>) userRepository.findAll();
    }

    @Override
    public void saveUser(AppUser user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<AppUser> findUserById(Long id) {
        return userRepository.findById(id);
    }
}