package com.authentic.gameo.service.impl;

import com.authentic.gameo.entity.Brand;
import com.authentic.gameo.repository.BrandRepository;
import com.authentic.gameo.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {
    
    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<Brand> getAllBrand() {
        return (List<Brand>) brandRepository.findAll();
    }

    @Override
    public void saveBrand(Brand user) {
        brandRepository.save(user);
    }

    @Override
    public void deleteBrand(Integer id) {
        brandRepository.deleteById(id);
    }

    @Override
    public Optional<Brand> findBrandById(Integer id) {
        return brandRepository.findById(id);
    }

    @Override
    public Brand getOne(Integer id) {
        return brandRepository.getOne(id);
    }

    @Override
    public List<Brand> saveAll(List<Brand> brands) {
        return brandRepository.saveAll(brands);
    }

    @Override
    public boolean existsBrandByBrandName(String brandname) {
        return brandRepository.existsBrandByBrandName(brandname);
    }
}