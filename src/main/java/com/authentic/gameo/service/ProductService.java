package com.authentic.gameo.service;

import com.authentic.gameo.entity.Category;
import com.authentic.gameo.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> getAllProduct();

    void saveProduct(Product product);

    void deleteProduct(Integer id);

    Optional<Product> findProductById(Integer id);

    Product getOne(Integer id);

    List<Product> saveAll(List<Product> product);

    boolean existsProductByProductName(String product);


}