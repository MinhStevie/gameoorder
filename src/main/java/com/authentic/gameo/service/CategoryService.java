package com.authentic.gameo.service;

import com.authentic.gameo.entity.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAllCategory();

    void saveCategory(Category category);

    void deleteCategory(Integer id);

    Optional<Category> findCategoryById(Integer id);

    Category getOne(Integer id);

    List<Category> saveAll(List<Category> category);

    boolean existsCategoryByCategoryName(String categoryname);
}