package com.authentic.gameo.service;

import com.authentic.gameo.entity.Brand;

import java.util.List;
import java.util.Optional;

public interface BrandService {
    List<Brand> getAllBrand();

    void saveBrand(Brand Brand);

    void deleteBrand(Integer id);

    Optional<Brand> findBrandById(Integer id);

    Brand getOne(Integer id);

    List<Brand> saveAll(List<Brand> brands);

    boolean existsBrandByBrandName(String brandname);
}