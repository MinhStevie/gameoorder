package com.authentic.gameo.service;

import com.authentic.gameo.entity.AppUser;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<AppUser> getAllUser();

    void saveUser(AppUser user);

    void deleteUser(Long id);

    Optional<AppUser> findUserById(Long id);
}