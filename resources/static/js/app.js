// import axios from 'axios';

// window.addEventListener('load', () => {

//   const api = 'http://www.colr.org/json/color/random';
//   const body = document.querySelector('body');

//   function randomColor() {
//     axios.get(api).then(res => {
//       let color = res.data.colors[0].hex;

//       if (!color) {
//         console.error('Random color could not be fetched.');
//       }

//       color = '#' + color;

//       body.style.backgroundColor = color;
//     }).catch(() => console.error('Random color could not be fetched.'));
//   }

//   randomColor();

//   setInterval(randomColor, 8000);

// });

(function ($) {

    console.log("Live");
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

    $(window).on('load', function () {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");

        $('.nav-item').on('click', function () {
            $('.nav-item').removeClass('active');
            $(this).addClass('active');
        });

    });

    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ') no-repeat center');
    });
})(jQuery);