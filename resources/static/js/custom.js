$(document).ready(function () {
    // $('.selectProduct').select2();

    $('#dataTables').DataTable({
        dom: 'Bfrtip',
        buttons: ['excel', 'pdf']
    });

    $('#dataTables tbody').on('click', 'tr', function () {

        if ($(this).hasClass('selected')) {
            $(this).toggleClass('selected');
        } else {
            $(this).toggleClass('selected');
        }
    });

    $('.modal').on('hidden.bs.modal', function () {
        $('.modal-body').empty();
    });

    // MODAL FOR CATEGORY

    $('#categoryCreate').on('show.bs.modal', function () {
        // var name = prompt("Please enter your name", "John Doe");
        $.get("/addcategory", function (data) {
            $('#categoryCreate').find('.modal-body').html(data);
        })
    });

    $('#categoryUpdate').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/editcategory?id=" + carid, function (data) {
            $('#categoryUpdate').find('.modal-body').html(data);
        })
    });

    $('#categoryDelete').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        console.log("deleteid is " + carid)
        $.get("/deletecategory?id=" + carid, function (data) {
            $('#categoryDelete').find('.modal-body').html(data);
        })
    });

    $('#categoryImport').on('show.bs.modal', function () {
        // var name = prompt("Please enter your name", "John Doe");
        $.get("/importcategory", function (data) {
            $('#categoryImport').find('.modal-body').html(data);
        })
    });

    // MODAL FOR BRAND

    $('#brandCreate').on('show.bs.modal', function () {
        // var name = prompt("Please enter your name", "John Doe");
        $.get("/addbrand", function (data) {
            $('#brandCreate').find('.modal-body').html(data);
        })
    });

    $('#brandUpdate').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/editbrand?id=" + carid, function (data) {
            $('#brandUpdate').find('.modal-body').html(data);
        })
    });

    $('#brandDelete').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/deletebrand?id=" + carid, function (data) {
            $('#brandDelete').find('.modal-body').html(data);
        })
    });

    $('#brandImport').on('show.bs.modal', function (e) {

        $.get("/importbrand", function (data) {
            $('#brandImport').find('.modal-body').html(data);
        })
    });

    // MODAL FOR PRODUCT

    $('#productCreate').on('show.bs.modal', function () {
        // var name = prompt("Please enter your name", "John Doe");
        $.get("/addproduct", function (data) {
            $('#productCreate').find('.modal-body').html(data);
            $('#productCreate').find('.modal-dialog').addClass('modal-lg');
        })
    });

    $('#productUpdate').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/editproduct?id=" + carid, function (data) {
            $('#productUpdate').find('.modal-body').html(data);
            $('#productUpdate').find('.modal-dialog').addClass('modal-lg');
        })
    });

    $('#productDelete').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/deleteproduct?id=" + carid, function (data) {
            $('#productDelete').find('.modal-body').html(data);
        })
    });

    $('#productImport').on('show.bs.modal', function (e) {

        $.get("/importproduct", function (data) {
            $('#productImport').find('.modal-body').html(data);
        })
    });

    // MODAL FOR Parameters

    $('#parametersCreate').on('show.bs.modal', function () {
        // var name = prompt("Please enter your name", "John Doe");
        $.get("/addparameters", function (data) {
            $('#parametersCreate').find('.modal-body').html(data);

        })
    });

    $('#parametersUpdate').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/editparameters?id=" + carid, function (data) {
            $('#parametersUpdate').find('.modal-body').html(data);
        })
    });

    $('#parametersDelete').on('show.bs.modal', function (e) {
        var carid = $(e.relatedTarget).data('id');
        $.get("/deleteparameters?id=" + carid, function (data) {
            $('#parametersDelete').find('.modal-body').html(data);
        })
    });
});